// ##############################
// // // Tasks for TasksCard - see Dashboard view
// #############################

var bugs = [
  'Mark has booked the Honda civic for 28-Aug-2020',
  "Dakota has booked the Tesla truck for 04-Sept-2020",
  "Minvera has booked the Honda civic for 28-Sept-2020",
  "Philip has booked the Ford for 27-Aug-2020"
];
var website = [
  "Minvera booking status has been changed to completed",
  "Dakota booking status has been changed to completed"
];
var server = [
  "Lines From Great Russian Literature? Or E-mails From My Boss?",
  "Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit",
  'Sign contract for "What are conference organizers afraid of?"'
];

module.exports = {
  // these 3 are used to create the tasks lists in TasksCard - Dashboard view
  bugs,
  website,
  server
};
